## Repo status 
development branch: [![build status](https://gitlab.com/ksator/network-infrastructure-as-code/badges/development/build.svg)](https://gitlab.com/ksator/network-infrastructure-as-code/commits/development)  

production branch: [![build status](https://gitlab.com/ksator/network-infrastructure-as-code/badges/production/build.svg)](https://gitlab.com/ksator/network-infrastructure-as-code/commits/production)  

## About this repo 
This repo is about Junos automation using a network as code approach. It covers CI/CD (continuous integration/continuous delivery), continuous delivery vs continuous deployment, multi stages pipelines delivery, git collaborative and approval workflows, test driven software development/green light software development, proactive automated tests, desired states enforcement using a declarative approach, ...

## Repo documentation
Visit [wiki](https://gitlab.com/ksator/network-infrastructure-as-code/wikis/home)   


